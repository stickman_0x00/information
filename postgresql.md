# Client

## [Password file](https://www.postgresql.org/docs/9.3/libpq-pgpass.html)

```bash
vim ~/.pgpass
```

```
hostname:port:database:username:password
```

```bash
chmod 600 ~/.pgpass
psql -h host -U someuser somedb
```
