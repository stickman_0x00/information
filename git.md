# Git

## Commands

### Remove all changes from previous commit

this will delete the last commit, attention all the changes will be deleted

```bash
git reset HEAD^ --hard
```

#### Remove last commit from remote

this will delete the last commit, attention all the changes will be deleted

```bash
git reset HEAD^ --hard
git push -f
```

#### Pull changes error “Commit your changes or stash them before you can merge”

```bash
git stash
git pull
git stash pop
```

### Git amend commit

```bash
git add $modifiedFile
git commit --amend
# without changing the message
git commit --amend --no-edit
# if commit already in remote
git push -f
```
