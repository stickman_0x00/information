# Fiori

## Local development

Vscode extension - SAPSE.sap-ux-fiori-tools-extension-pack

```bash
npm install -g yo
npm install -g generator-easy-ui5
npm install -g @sap/generator-fiori
npm install -g mbt
```

## Courses

- [Set Up SAP Fiori Tools in Your Development Environment](https://developers.sap.com/tutorials/fiori-tools-vscode-setup.html)
- [Develop Your First SAPUI5 Web App on Cloud Foundry](https://developers.sap.com/mission.sapui5-cf-first.html)

### openSAP

- [Developing Web Apps with SAPUI5](https://open.sap.com/courses/ui51)
- [Developing and Extending SAP Fiori Elements Apps](https://open.sap.com/courses/fiori-ea1)
- [Evolved Web Apps with SAPUI5](https://open.sap.com/courses/ui52)
- [Developing and Extending SAP Fiori Elements Apps](https://open.sap.com/courses/fiori-ea1)
- [SAP Fiori Overview: Design, Develop and DeploySAP Fiori Experts](https://open.sap.com/courses/fiori3)

## Videos

- [FioriConf 2024](https://www.youtube.com/watch?v=0DleJ1jbgLY)
- [Boost Your Development Experience with UI5 Tooling Extensions](https://www.youtube.com/watch?v=UDFAadHyAbE&t=1154s)

### Set outisde API

- [SAPUI5: Remote Data Source, Approuter (#4)](https://www.youtube.com/watch?v=MHJzIz7iiHs)
- [SAPUI5: Approuter, Middleware (#5)](https://www.youtube.com/watch?v=cy9oolXAJDE)

## Typescript

### SAP

- [UI5 & TypeScript](https://sap.github.io/ui5-typescript/#how-to-set-up-a-new-ui5-app-for-typescript-development)

### Github

- [A Small TypeScript UI5 Example App](https://github.com/SAP-samples/ui5-typescript-helloworld)
- [ui5-typescript-tutorial - Learn App Development in UI5 with TypeScrip](https://github.com/SAP-samples/ui5-typescript-tutorial)
- [Converting UI5 Apps from JavaScript to TypeScript](https://github.com/SAP-samples/ui5-cap-event-app/blob/typescript/docs/typescript.md#converting-ui5-apps-from-javascript-to-typescript)
- [TypeScriptDemo](https://github.com/lemaiwo/UI5TypeScriptDemoApp/tree/main)
