# SAP

## Resources

- [Guides and Tutorials for ABAP AS, Developer Edition](https://blogs.sap.com/2014/02/06/guides-and-tutorials-for-the-developer-edition-of-as-abap-incl-bw-on-sap-hana/)
- [ABAP Development](https://community.sap.com/topics/abap)
- [SAP HANA Developer Guide](https://help.sap.com/doc/PRODUCTION/fbb802faa34440b39a5b6e3814c6d3b5/2.0.00/en-US/SAP_HANA_Developer_Guide_for_SAP_HANA_Studio_en.pdf)
- [SAP Application Extension Methodology Overview](https://help.sap.com/docs/sap-btp-guidance-framework/sap-application-extension-methodology/sap-application-extension-methodology-overview)
