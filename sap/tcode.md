# Transactions

## Tables

| Transaction | Name                              | Additional information |
| ----------- | --------------------------------- | ---------------------- |
| SE11        | ABAP Dictionary                   |                        |
| SE14        | ABAP Dictionary: Database Utility | Adjust                 |
| SE16        | Data Browser                      |                        |
| SM30        | Edit Table Views                  | Maintenance Views      |
| SNRO        | Number Range Object Editing       |                        |

## Code

| Transaction  | Name                   |
| ------------ | ---------------------- |
| SE80         | Object Navigator       |
| SE37         | Function Builder       |
| SE38         | ABAP Editor            |
| SE24         | Class Builder          |
| SE51         | Screen Painter         |
| BSP_WD_CMPWB | UI Component Workbench |

Note: BSP_WD_CMPWB > AIC_CMCD_H - Solman UI

### Analysis

| Transaction | Name                            | Additional information                |
| ----------- | ------------------------------- | ------------------------------------- |
| SAT         | Runtime Analysis                |                                       |
| ST22        | ABAP Runtime Errors             | Dumps                                 |
| ST05        | Performance Trace               | Trace                                 |
| SRTCM       | Runtime Check Monitor           | Find potential functional regressions |
| SWLT        | SQL Performance Tuning Worklist |                                       |

### Code Inspector and Testing

| Transaction | Name              |
| ----------- | ----------------- |
| SCI         | Code Inspector    |
| ATC         | ABAP Test Cockpit |

### Business Object Framework

| Transaction | Name                                 |
| ----------- | ------------------------------------ |
| BOBT        | Business Object Builder: Test        |
| BOBX        | Business Object Processing Framework |

### Run code

| Transaction | Name                    |
| ----------- | ----------------------- |
| SA38        | ABAP: Program Execution |

### OData and Web Services

| Transaction            | Name                           |
| ---------------------- | ------------------------------ |
| SEGW                   | SAP Gateway Service Builder    |
| /n/IWFND/MAINT_SERVICE | Activate and Maintain Services |

#### Errors

| Transaction        | Name                   | Additional information |
| ------------------ | ---------------------- | ---------------------- |
| /n/IWBEP/ERROR_LOG | SAP Backend Error Log  | Backend                |
| /n/IWFND/ERROR_LOG | SAP Gateway: Error Log | Frontend               |

### User Exits and Enhancements

| Transaction | Name                                     | Additional information       |
| ----------- | ---------------------------------------- | ---------------------------- |
| SE18        | BAdI Builder                             | Definitions                  |
| SE19        | BAdI Builder                             | Implementations              |
| SE20        | Enhancements                             | Repository browser for BAdIs |
| SMOD        | SAP Enhancements                         |                              |
| CMOD        | Project Managements for SAP Enhancements |                              |
| SE93        | Maintain Transaction                     |                              |

## Transports

| Transaction | Name                      | Additional information                              |
| ----------- | ------------------------- | --------------------------------------------------- |
| SE10        | Transport Organizer       | Maintain transports (main)                          |
| SE01        | Transport Organizer       | Display and maintain transport logs (Extended View) |
| SE03        | Transport Organizer Tools |                                                     |

## Authorization | Security | Configuration

| Transaction | Name                                |
| ----------- | ----------------------------------- |
| SU53        | Display Authorization Data for User |
| SU21        | Maintain Authorization Objects      |
| PFCG        | Role Maintenance                    |
| SICF        | Define Services                     |
| SM59        | Configuration of RFC Connections    |
| SM56        | Number Range Buffer                 |

## Documentation and Forms

| Transaction | Name           |
| ----------- | -------------- |
| SE61        | Edit Documents |

## System Info and Patching

| Transaction | Name                    |
| ----------- | ----------------------- |
| SPAM        | Support Package Manager |

## Table Logs and Tracing

| Transaction | Name                                           |
| ----------- | ---------------------------------------------- |
| SCU3        | Analyze Changed Customizing Objects and Tables |

## Fiori

| Transaction      | Name                                       | Additional information |
| ---------------- | ------------------------------------------ | ---------------------- |
| LPD_CUST         | Overview of Launchpads                     |                        |
| /n/UI2/FLPD_CONF | Fiori Launchpad Designer                   | Client Independent     |
| /n/UI2/FLPD_CUST | Fiori Launchpad Designer                   | Client Dependent       |
| /n/UI2/FLP       | Fiori Launchpad                            |                        |
| /n/UI2/FLIA      | Fiori Launchpad Intent Resolution Analysis |                        |
| /n/UI2/FLC       | Fiori Launchpad Checks                     | Find information of catalog |

| Program                  | Description                                                        |
| ------------------------ | ------------------------------------------------------------------ |
| /UI5/UI5_REPOSITORY_LOAD | Upload, Download, or Delete Apps to or from SAPUI5 ABAP Repository |

### Debug

Debug Launchpad: Ctrl + alt + shift + f/s

### Clean cache

| Transaction            | Name                   | Additional information |
| ---------------------- | ---------------------- | ---------------------- |
| /n/IWFND/CACHE_CLEANUP | Cleanup of Model Cache | Frontend               |
| /n/IWBEP/CACHE_CLEANUP | Cleanup of Model Cache | Backend                |

| Program                       | Description                                        |
| ----------------------------- | -------------------------------------------------- |
| /UI5/APP_INDEX_CALCULATE      | Calculation of SAPUI5 Application Index            |
| /UI2/INVALIDATE_CLIENT_CACHES | Client Cache Invalidation                          |
| /UI2/DELETE_CACHE_AFTER_IMP   | Delete UI2 Cache after import of a Support Package |
| /UI2/INVALIDATE_GLOBAL_CACHES | Global Cache Invalidation                          |

## Email

| Transaction | Name               | Additional information         |
| ----------- | ------------------ | ------------------------------ |
| SCOT        |                    | SAPconnect administration tool |
| SOST        | SAPconnect         | Monitor outgoing emails.       |
| SBWP        | Business Workplace |                                |

## Users

| Transaction | Name                  | Additional information |
| ----------- | --------------------- | ---------------------- |
| SU01        | User Maintenance      |                        |
| SU01D       | User Maintenance      | Read only              |
| SU01N       | User Maintenance      |                        |
| SU3         | Maintain User Profile |                        |

## System upgrade

| Transaction | Name                             | Additional information    |
| ----------- | -------------------------------- | ------------------------- |
| SPDD        | Software Package Data Dictionary | Adjust dictionary objects |
| SPAU        |                                  | Adjust repository objects |

## Documentation

| Transaction | Name                        | Additional information |
| ----------- | --------------------------- | ---------------------- |
| OAOR        | Business Document Navigator | Add documentation      |

## HR

| Transaction | Name                    | Additional information |
| ----------- | ----------------------- | ---------------------- |
| PA10        | Personnel file          |                        |
| PA20        | Display HR Master Data  |                        |
| PA30        | Maintain HR Master Data |                        |

## Cluster

| Transaction | Name                    | Additional information |
| ----------- | ----------------------- | ---------------------- |
| SM34        | View Cluster Editing    |                        |

Notes: AIC_SETTINGS	Settings for Change Request Management in the CRM Web UI


## Resources

- [How to find which Transaction code that is replaced by a new Transaction code in SAP S/4 HANA?](https://blogs.sap.com/2020/07/15/how-to-find-which-transaction-code-that-is-replaced-by-a-new-transaction-code-in-sap-s-4-hana/)
