# SAP NetWeaver AS ABAP Developer Edition Configurations

# Optimizations

## DB OPTIMIZATION

1. Log into the SAP system with SAP GUI using the user DEVELOPER, client 001 and your password
2. Call transaction **DBCO**
3. Switch to the **Change mode** (Ctrl + F1)
4. Select database connection **+++SYBADM** and click Goto → Details
5. Enter the correct password of SAP ASE database user **sapsa**
6. Hit the **Save** button (Ctrl + S)
7. Switch to the **View mode**

## SPEED UP DB

### First extend memory

1. Log into the operating system as user **sybnpl**
2. Run the command **isql -Usapsa -X -SNPL**
3. Run the command **sp_configure 'max memory',740621**
4. Run the command **go**
5. Run the command **quit**

### SAP ASE: Deactivating Granular Permissions

1. Log into the operating system as user **sybnpl**
2. Run the command **isql -Usapsso -X -SNPL**
3. Run command **sp_configure 'granular permission', 0**
4. Run the command **go**
5. Run the command **quit**

### Turn off SAP ASE: Auditing

1. Log into the operating system as user **sybnpl**
2. Run the command **isql -Usapsso -X -SNPL**
3. Run command **sp_configure 'auditing', 0**
4. Run the command **go**
5. Run the command **quit**

# Resources

- [Power up your SAP NetWeaver Application Server ABAP Developer edition](https://blog.maruskin.eu/2019/11/)