# Development

# Table IDS

## ID with number range

### Creation

[How to Create you own Number Range SNRO](https://wiki.scn.sap.com/wiki/display/ABAP/How+to+Create+you+own+Number+Range++SNRO)

1. Log to Transaction Code SNRO.
2. Number Range Object Creation.
3. Set Number Length Domain (Subobject Data Elemente OPTIONAL).
4. Set % Warning
5. Save
6. Interval Editing
7. Intervals
8. Add interval
9. Save

### ABAP

```abap
DATA: lv_rc TYPE inri-returncode.

CALL FUNCTION 'NUMBER_GET_NEXT'
  EXPORTING
    nr_range_nr             = '...'
    object                  = '...'
  IMPORTING
    number                  = ...
    returncode              = lv_rc
  EXCEPTIONS
    interval_not_found      = 1
    number_range_not_intern = 2
    object_not_found        = 3
    quantity_is_0           = 4
    quantity_is_not_1       = 5
    interval_overflow       = 6
    buffer_overflow         = 7
    OTHERS                  = 8.
IF sy-subrc <> 0.
  MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
          WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
  RETURN.
ENDIF.
```

#### Return Code (lv_rc)

1. lv_rc = ' ' - Everything is working fine
2. lv_rc = '1' - The assigned number lies in the critical area.
3. lv_rc = '2' - This was the last number, next time the first number will be assigned

## GUID

```abap
DATA(lv_guid) = cl_system_uuid=>create_uuid_x16_static( ).
```

# Exceptions

## Message

```abap
CALL FUNCTION '...'
  ...
  EXCEPTIONS
    ...
  .
IF sy-subrc <> 0.
  MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
          WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
ENDIF.
```

# Generate randoms

## String

FM GENERAL_GET_RANDOM_STRING

## UUID

CL cl_system_uuid

# HANA

## Checks (SCI/ATC)

Variants:

- Potential functional regressions (FUNCTIONAL_DB)
- Potential performance optimization (PERFORMANCE_DB)

# Show info

```abap
" ALV
cl_salv_gui_table_ida=>create( iv_table_name = 'T000' )->fullscreen( )->display( ).

" Simple display
cl_demo_output=>display( LT_TEST ).

" With data
cl_demo_output=>next_section( title = |Test title| ).
cl_demo_output=>write_data( name = 'Test data'  value = LT_TEST ).
cl_demo_output=>display( ).
```

# Timer

```abap
DATA(lo_timer) = cl_abap_runtime=>create_hr_timer( ).
DATA(t1) = lo_timer->get_runtime( ).
DATA(t2) = lo_timer->get_runtime( ).
DATA(elapsed_time) = ( t2 - t1 ) / 1000. " / 1000 for ms
```

# Check if record exists

```abap
"DATA lv_exists type abap_bool value abap_false.

SELECT SINGLE @abap_true
  FROM T000
  WHERE MANDT = '002'
  INTO @DATA(lv_exists).
IF lv_exists EQ ABAP_TRUE.
  WRITE:/ 'Exists.'.
ENDIF.

" No variable
SELECT COUNT( * )
  FROM T000
  WHERE MANDT = '001'.
IF sy-subrc EQ 0.
  WRITE:/ 'Exists.'.
ENDIF.
```

# [Open SQL Expressions](https://help.sap.com/doc/abapdocu_740_index_htm/7.40/en-US/index.htm?file=ABAPSQL_EXPR.htm)

## Resources

- [How to enable clean code checks for ABAP](https://blogs.sap.com/2022/05/05/how-to-enable-clean-code-checks-for-abap/?source=social-Global-YOUTUBE-Developer_Community-Developers-Business_Technology_Platform_Umbrella-spr-11381551217-account_name&campaigncode=CRM-XB23-MKT-DGECOMYD&sprinklrid=11381551217)
- [Clean code checks for ABAP – Cloud Edition](https://blogs.sap.com/2023/09/11/clean-code-checks-for-abap-cloud-edition/)

## Learning

- [Build an SAP Fiori App Using the ABAP RESTful Application Programming Model [RAP100]](https://developers.sap.com/mission.sap-fiori-abap-rap100.html)

## IDE/Enviroment/Tools

- [ABAP Tools - Configure Tool Bridge](https://software-heroes.com/en/blog/abap-tools-work-with-ecplise-configure-tool-bridge)
- [SAP Fiori tools May 2024 release adds closer integration with ABAP Development Tools](https://community.sap.com/t5/technology-blogs-by-sap/sap-fiori-tools-may-2024-release-adds-closer-integration-with-abap/ba-p/13720761)

## abap2UI5

- [abap2UI5](https://github.com/abap2UI5/abap2UI5)
- [abap2UI5 - Integration with SAP Business Technology Platform (1/3) - Installation & Configuration](https://www.linkedin.com/pulse/abap2ui5-integration-sap-business-technology-platform-13-installation-lf1re/)
