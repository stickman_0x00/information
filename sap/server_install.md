# SAP NetWeaver AS ABAP Developer Edition Installation

## Requirements

### Download files

- [SAP NetWeaver AS ABAP Developer Edition](https://developers.sap.com/trials-downloads.html?search=abap)
- [Debian](https://www.debian.org/)

### Decompress files of SAP NetWeaver AS ABAP Developer Edition

## Install OS

### Install Debian minimal

Hostname: **vhcalnplci**

Domain: **dummy.nodomain**

### Install dependencies for sap script

```bash
sudo apt install csh uuid-runtime libaio1
```

### Change line on /etc/hosts to have current ip

```
sudo vim /etc/hosts
    xxxx.xxxx.xxxx.xxxx vhcalnplci.dummy.nodomain vhcalnplci
```

### Reboot

A reboot is recomended but not necessary, but it will be required to start the **uuid** service manually.

## Install SAP service

### Prepare installation files

1. Copy license file to the right place.

```bash
cp  ./License/SYBASE_ASE_TestDrive/SYBASE_ASE_TestDrive.lic <version>/server/TAR/x86_64/
```

2. [Adjusting installer script](https://blogs.sap.com/2021/06/07/adjusting-installer-script-for-sap-netweaver-dev-edition-for-distros-with-kernel-version-5.4-or-higher/)

On install.sh script replace this code:

```bash
./saphostexec -install || do_exit $ERR_install_saphost

# TODO: is it ok to remove /tmp/hostctrl?
cd /
rm -rf /tmp/hostctrl || log_echo "Failed to clean up temporary directory"
```

With this:

```bash
#Replace this line with one which tries to continue (this) main script using ‘&’:
#./saphostexec -install || do_exit $ERR_install_saphost
./saphostexec -install &

#Wait for a while so that hopefully the asynchronous call ends:
log_echo "Waiting 30 seconds for asynchronous call to /tmp/hostctrl/saphostexec -install to complete..."
sleep 30
log_echo "30 seconds are up, continuing the main script."

# TODO: is it ok to remove /tmp/hostctrl?
cd /
#Let's not remove the temporary directory, in case saphostexec command
#is still executing. So commenting out:
# rm -rf /tmp/hostctrl || log_echo "Failed to clean up temporary directory"

# Now we modify the RUN_NPL executable (executable permissions are for sybnpl user):
FILENPL=/sybase/NPL/ASE-16_0/install/RUN_NPL
if test -f "$FILENPL"; then
    echo "$FILENPL exists. Adding the -T11889 option to config in that file:"
    sed -i 's/NPL.cfg \\/NPL.cfg -T11889 \\/g' /sybase/NPL/ASE-16_0/install/RUN_NPL
    cat $FILENPL
    echo "-T11889 config option added"
    sleep 15
else
    echo "$FILENPL does not exist. Not modifying what doesn’t exist, ontologically seems ok."
fi
```

### Run script

Important or will fail, [password rules](https://help.sap.com/doc/saphelp_nw73ehp1/7.31.19/en-us/4a/c3efb58c352470e10000000a42189c/content.htm?no_cache=true). The password provided will be the same password for the new user(npladm) of the system.

```bash
chmod +x install.sh
sudo ./install.sh
```

## SAP Client

### Installation of client (Optional)

#### Windows

On the server files you have also the client files.\
`<version>\client\JAVAGUI`\
`<version>\client\SAPGUI4Windows`

Choose one of the versions, uncompress the zip and execute the file.\
Example of SAPGUI4Windows:
`"C:\Users\qwe\Downloads\50144807_6\BD_NW_7.0_Presentation_7.50_Comp._2_\PRES1\GUI\WINDOWS\Win32\SetupAll.exe"`

#### Linux (Debian)

Install dependencies

```bash
sudo apt install default-jre openjfx
```

Run and install

```bash
java -jar <version>/client/JAVAGUI/PlatinGUI***_*-********.JAR
```

Module path: `/usr/share/openjfx/lib`

### Connection to the server

#### User

Client: **001**\
Users: **DEVELOPER**\
Password: **Down1oad**

#### Administrator

Client: **001**\
Users: **SAP\***\
Password: **Down1oad**

#### SAPGUI4Windows

Application server: **<SERVER_IP>**\
Instance Number: **00**\
System ID: **NPL**

#### JAVAGUI

Connection string `conn=/H/<server_ip>/S/3200`

## Post-Installation

### ABAP License key

Go to transaction **SLICENSE**. \
Copy the **Active Hardware Key**.\
Get new license from [here](https://go.support.sap.com/minisap/#/minisap).\
Delete the old license **NetWeaver_SYB** and install the new one.

## Users

For more information of existing users check it [here](https://blogs.sap.com/2019/10/01/as-abap-7.52-sp04-developer-edition-concise-installation-guide/) or on the readme.html file that is provided with the installation files.

## Useful commands

OS User: **npladm**\
Password: **<password_of_script>**

```csh
startsap ALL
stopsap ALL
sapcontrol -nr 00 -function GetProcessList
```

## Resources

### Server installation

- [AS ABAP 7.52 SP04, Developer Edition: Concise Installation Guide](https://blogs.sap.com/2019/10/01/as-abap-7.52-sp04-developer-edition-concise-installation-guide/)
- [SAP NetWeaver AS ABAP Developer Edition Installation Pitfalls + Video Tutorial](https://blogs.sap.com/2022/11/14/sap-netweaver-as-abap-developer-edition-installation-pitfalls/)
- [How to Install FREE SAP System for Learning ABAP](https://abapacademy.com/blog/how-to-install-free-sap-system/)
- [SAP Trial license expired? How to prolong SAP Trial license?](https://abapacademy.com/blog/logon-not-possible-error-in-license-check-how-to-prolong-sap-trial-license/)
- [Installing AS ABAP 7.52 dev edition on Virtual Box and Linux](https://www.sap.com/documents/2019/09/32638f18-687d-0010-87a3-c30de2ffd8ff.html)

### JavaGUI

- [SAP GUI for the Java Environment](https://help.sap.com/doc/f540a730ff3c46a29c34be1fd3cd3275/780.00/en-US/sap_gui_for_java.pdf)
- [SAP GUI For Java Connection Strings](https://blogs.sap.com/2017/08/01/sap-gui-for-java-connection-strings/)
- [Using Direct Connections](https://help.sap.com/docs/sap_gui_for_java/e665f2b67dbd4328ab6bd9e029b84581/dd2cc51feed14ab6ab87e00c483881f3.html)

### Others

- [Guides and Tutorials for ABAP AS, Developer Edition](https://blogs.sap.com/2014/02/06/guides-and-tutorials-for-the-developer-edition-of-as-abap-incl-bw-on-sap-hana/)
