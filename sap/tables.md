# Tables

| Table | Description                                     |
| ----- | ----------------------------------------------- |
| CVERS | Release Status of Software Components in System |
| T00   | Clients                                         |
| USR01 | User master record (runtime data)               |
| NRIV  | Number Range Intervals                          |
