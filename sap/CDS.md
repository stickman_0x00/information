# ABAP Core Data Services (CDS)

## Check if feature can be used

```abap
DATA(lv_feature_supported) = cl_abap_dbfeatures=>use_features(
  EXPORTING
  requested_features = VALUE #( ( cl_abap_dbfeatures=>itabs_in_from_clause ) )
).

IF lv_feature_supported EQ ABAP_TRUE.
  " SUPPORTED
ENDIF.
```

## Resources

- [SAP TechBytes -- ABAP Core Data Services Overview #1](https://www.youtube.com/watch?v=lvorIY4Xyio)
- [SAP TechBytes — ABAP Core Data Services - Virtual Elements #2](https://www.youtube.com/watch?v=TqD-H03znVo)
- [SAP TechBytes -- ABAP CDS View Extensions #3](https://www.youtube.com/watch?v=IPEotH-6ekI)
- [SAP TechBytes -- ABAP CDS Access Control #4](https://www.youtube.com/watch?v=uxgs4FigmuQ)
- [SAP TechBytes — ABAP Core Data Services Unit Test #5](https://www.youtube.com/watch?v=ezQ0vbhV8QY)
- [SAP TechBytes — ABAP Core Data Services Troubleshooting #6](https://www.youtube.com/watch?v=q7Yhj6BnWJo)
- [SAP TechBytes — ABAP Core Data Services Annotations #7](https://www.youtube.com/watch?v=GXFHjq5L8M8)
- [SAP TechBytes — ABAP Core Data Services Associations #8](https://www.youtube.com/watch?v=fhmx51FIysE)
- [Cheat Sheet PDF](https://d.dam.sap.com/a/UgpGkLH/Cheat_Sheet.pdf?rc=10&inline=true&doi=SAP1083288)
- [Cheat Sheet Blog](https://www.brandeis.de/en/blog/cheat-sheet-cds-abap)
