# ABAP

## Resources

- [Advanced New ABAP Part 1](https://www.linkedin.com/pulse/advanced-new-abap-george-drakos)
- [Advanced New ABAP Part 2](https://www.linkedin.com/pulse/advanced-new-abap-part-2-george-drakos)

### Videos

- [Advanced ABAP - A practical Showcase](https://www.youtube.com/watch?v=9TMvn90Oa1Q)
