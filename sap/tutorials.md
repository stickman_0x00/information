# Tutorials

# ABAP

## Blog

-   [ABAP 7.40 Quick Reference](https://blogs.sap.com/2015/10/25/abap-740-quick-reference/)
-   [ABAP Built-in Functions](https://blogs.sap.com/2015/11/30/reminder-abap-built-in-functions/)
-   [Clean ABAP](https://github.com/SAP/styleguides/blob/main/clean-abap/CleanABAP.md)

## Youtube

-   [ABAP](https://www.youtube.com/playlist?list=PL6RpkC85SLQB-vyEFpUj1xkrIhH4UiV4D)
-   [ABAP Object Oriented Concepts](https://www.youtube.com/playlist?list=PL6RpkC85SLQCGjMBsoQYlMrLmbZaEWM6U)

# BTP

## Youtube

-   [SAP Business Technology Platform Onboarding](https://www.youtube.com/playlist?list=PLkzo92owKnVw3l4fqcLoQalyFi9K4-UdY)

# CAP

## Youtube

-   [Back to basics: Managed associations in SAP Cloud Application Programming Model](https://www.youtube.com/playlist?list=PL6RpkC85SLQCSm1JSRzeBE-BlkygKRAAF)

## Git

[Exploring and understanding managed associations in the SAP Cloud Application Programming Model](https://github.com/qmacro/managed-associations-in-cap/)
