# Client

## Create key

```bash
ssh-keygen -t ed25519 -C "$(user) key" -f ~/.ssh/$(file)
```

## Send pubkey to server

```bash
ssh-copy-id -i ~/.ssh/$(file).pub $(user)@$(ip)
```


## Client Configuration

```bash
vim .ssh/config
    Host $(server_name)
        User $(user)
        Hostname $(server_ip)
        Port $(server_port)
        # secure mode of accessing some admin site without open port on firewall ^^
        # no ssl required too
        #LocalForward 127.0.0.1:xx 127.0.0.1:xx
        IdentityFile ~/.ssh/$file
```

# Server

## Configuration

```bash
vim /etc/ssh/sshd_config
# Allow users
    AllowUsers $(user)
# Change port for "security" + easy for the logs
	Port $(server_port)
# Only allow pubkey to be used
    PubkeyAuthentication yes
    AuthorizedKeysFile  .ssh/authorized_keys
    PasswordAuthentication no
# Don't allow root login MANDATORY
    PermitRootLogin no
    MaxAuthTries 3
    X11Forwarding no
```

## Two Auth

### Install

```bash
# install package libpam-google-authenticator
# optional for scannable QR code - package qrencode
```

### Generate

```bash
google-authenticator
# answer yes to all except
# Do you want to disallow multiple uses...
# By default, a new token is generated every 30 seconds by the mobile app.
```

### Configure

```bash
vim /etc/pam.d/sshd
    auth required pam_google_authenticator.so

vim /etc/ssh/sshd_config
    ChallengeResponseAuthentication yes
# Arch Linux
    AuthenticationMethods publickey,keyboard-interactive:pam
```

# Utils

## Copy files ssh

```bash
# remote to local
scp remote_user@remote_host:/path/to/remote/file /path/to/local/file
# local to remote
scp File.zip remote_user@remote_host:~/
```

## Check if you are a ssh_client

```bash
echo $SSH_CLIENT
```