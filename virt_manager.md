# KVM

## 1. - Create Windows 11 VM

### 1.1 Customize configuration before install

#### 1.1.6 Firmware

Overview > Firmware - UEFI x86_64: /usr/share/edk2/x64/OVMF_CODE.secboot.4m.fd

#### 1.1.2 - Clock

- Overview > XML > domain.clock.timer (name=rtc & pit) remove
- Overview > XML > domain.clock.timer (name=hpet) to yes

#### 1.1.3 - CPUs

CPUs > Details > Topology - Convert sockets to Cores

#### 1.1.4 VirtIO Disk

VirtIO Disk 1 > Details > Disk Utils - VirtIO

#### 1.1.5 Network Card

NIC :xx:xx:xx > Details > Device model - VirtIO

#### 1.1.6 Download [Drivers](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/stable-virtio/)

Download virtio-win.iso

- Add Hardware > Storage > Select or create... > Manage - Choose virtio-win.iso

### 1.2 During install

#### 1.2.1 Load driver

Custom Install > Load Driver > Ok > Choose W11

#### 1.2.2 Let's connect you to a network screen

Shift + F10 > OOBE\BYPASSNRO

### 1.3 After Install

#### 1.3.1 Install Guest Agent

- E:/
- Install virtio-win-guest-tools
- Install virtio-win-gt-x64
- Reboot

#### Dark theme

C:\Windows\Resources\Themes

## 2. Mount filesystem

### 2.1 Host(Linux) to Client(Linux)

Add Filesystem.

```bash
sudo mount -t virtiofs share_name /mnt/
```

### 2.2 Host(Linux) to Client(Windows)

1. Memory > Enable Share Memory.

1. Add Filesystem.

1. Downoad WinFSP and Install.

1. Services > VirtIO-FS Service > Startup Type "Automatic" > Start > Apply > Ok.
