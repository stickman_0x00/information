# Run Gui applications X Server (Example gimp)

## Allow any client, xhost

```bash
xhost +
```

## Dockerfile

```dockerfile
FROM ubuntu
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y gimp
```

### Build and run

```bash
docker build . -t gimp
docker run --rm -it --name gimp -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix:ro gimp
```

### Revert xhost

```bash
xhost -
```