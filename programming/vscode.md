# Extensions

# General

- [Code Spell Checker](https://open-vsx.org/extension/streetsidesoftware/code-spell-checker)
- [Git Graph](https://open-vsx.org/extension/mhutchie/git-graph)
- [REST Client](https://open-vsx.org/extension/humao/rest-client)
- [Todo Tree](https://open-vsx.org/extension/Gruntfuggly/todo-tree)
- [Error Lens](https://open-vsx.org/extension/usernamehw/errorlens)
- [Docker](https://open-vsx.org/extension/ms-azuretools/vscode-docker)

## Themes

- [SynthWave '84](https://open-vsx.org/extension/RobbOwen/synthwave-vscode)
- [Material Icon Theme](https://open-vsx.org/extension/PKief/material-icon-theme)


# C/C++

- [C/C++](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)

# Rust

- [Better TOML](https://open-vsx.org/extension/bungcip/better-toml)
- [rust-analyzer](https://open-vsx.org/extension/rust-lang/rust-analyzer)
- [crates](https://open-vsx.org/extension/serayuzgur/crates)
- [CodeLLDB](https://open-vsx.org/extension/vadimcn/vscode-lldb)

# Go

- [Go](https://open-vsx.org/extension/golang/Go)
