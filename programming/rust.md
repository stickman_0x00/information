# Rust

# Interactive learning environments

## [Rust Playground](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021)

## [Interactive Rust in a REPL and Jupyter Notebook with EVCXR](https://depth-first.com/articles/2020/09/21/interactive-rust-in-a-repl-and-jupyter-notebook-with-evcxr/)

```bash
cargo install evcxr_repl
evcxr
>> let mut s = String::from("Hello, ");
>> s.push_str("World!");
> s
"Hello, World!"
```

# Compiler

## [Compiler Explorer](https://rust.godbolt.org/)

# Learning

## Tour

-   [Tour of Rust](https://tourofrust.com/)

## Exercieses

-   [rustlings](https://github.com/rust-lang/rustlings)

## Courses

-   [Take your first steps with Rust](https://learn.microsoft.com/en-us/training/paths/rust-first-steps/)

## Videos

-   [Easy Rust / Rust in a Month of Lunches: bite-sized Rust tutorials](https://www.youtube.com/playlist?list=PLfllocyHVgsRwLkTAhG0E-2QxCf-ozBkk)

# Resources

-   [Rust by Example](https://doc.rust-lang.org/rust-by-example/)
-   [Learn Rust in X Minutes](https://learnxinyminutes.com/docs/rust/)
-   [Rosetta Code](https://rosettacode.org/)
-   [Clippy Lints documentation](https://rust-lang.github.io/rust-clippy/stable/index.html)
-   [](https://dhghomon.github.io/easy_rust/)

# Tips

[5 Better ways to code in Rust](https://www.youtube.com/watch?v=BU1LYFkpJuk)
