# OpenWrt Install and Configuration

## [Accessing LuCI Web Interface Securely]("https://openwrt.org/docs/guide-user/luci/luci.secure")

server

```bash
ssh root@openwrt
# Backup uhttp
cp /etc/config/uhttpd /etc/config/uhttpd.original
# Comment all lines that start by list "list listen_http"
vim /etc/config/uhttpd
    config uhttpd main
        list listen_http '127.0.0.1:80'
/etc/init.d/uhttpd restart
```

client

```bash
ssh -L127.0.0.1:8000:127.0.0.1:80 root@openwrt
```

[localhost:8000](http://localhost:8000)

## Update from terminal

```bash
opkg update
opkg list-upgradable | cut -f 1 -d ' ' | xargs opkg upgrade
```

## Set ssh-key
