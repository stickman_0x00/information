# VIM

## Create window

<Crtl>+<w>+
s - open window on the right
v - open window below
:sp <file>
:vs <file>

## Move to other window

<Crtl>+<w>+<arrow>

## Find/Replace Word

:set hlsearch - highlighting on
:noh - highlighting
:%s/find/sub - substitui as palavras

## Go back to previous file

Ctrl + o

## Spellchecking

[s - next mistake
]s - previous mistake
zg - add to the dictionary
zw - remove from dictionary

# Netrw

d - create directory
% - create file
