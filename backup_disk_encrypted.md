# Encrypt disk with LUKS for backups

## Create disk

Attach a new device which will show as /dev/sdX.

Use fdisk to delete any partitions on it and create a new one of type linux.

```bash
fdisk /dev/sdX
```

Use cryptsetup to format the new partition.

```bash
cryptsetup luksFormat /dev/sdX1
```

Use cryptsetup to open the device.

```bash
cryptsetup open /dev/sdX1 my_encrypted_disk
```

Format the newly created disk.

```bash
mkfs.ext4 /dev/mapper/my_encrypted_disk
```

## Use disk

Use cryptsetup to open the device.

```bash
cryptsetup open /dev/sdX1 my_encrypted_disk
```

Mount the disk.

```bash
mount /dev/mapper/myEncryptedDisk /mnt
```

### Example backup (home)

Create file to ignore files and folders.

```bash
vim .ignore_list
```

```bash
# Directories
.cache
.local/share/Trash

# Files

.xsession-errors
```

```bash
# -x (do not look inside mount points)
# --delete: Deletes files in the destination directory if they don't exist in the source directory.
rsync -x -avP --delete --exclude-from=.ignore_list /home/$USER /mnt/
```

## Close disk

Unmount

```bash
umount /mnt
```

Close the encrypted partition.

```bash
cryptsetup close my_encrypted_disk
```

## Simple home backup

```bash
rsync -x -aqP --delete --exclude=.cache/ /home/$USER /mnt/
```
