# Files

# Create files with different creation dates

```bash
>seq 1 1000 | xargs -n1 /bin/sh -c 'date --date="$0 days ago" +"%Y%m%d"' | xargs -n1 /bin/sh -c 'touch -d $0 test_$0.txt'
```
