# Disks

# Fix ntfs

```bash
sudo ntfsfix -b -d /dev/sdx
```

# Burn IMG/ISO

## Burn img/iso to disk

>CHECK THE DISK 1 2 3 4 5 TIMES !!!

```bash
dd bs=4M if=$iso of=$disk conv=fsync status=progress
# if don't work use " bs=1M " More Slow
```

# Zero disk
sudo dd if=/dev/zero of=/dev/sdX conv=fsync status=progress bs=1M
