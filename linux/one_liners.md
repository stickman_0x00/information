# One liners

# Python

## Web Server

## 0.0.0.0

```bash
python -m http.server 8000
```

## Specific IP

```bash
python -m http.server 8000 --bind $ip
```