# After Install Debian + Mate + Standard Libraries

## Add user to sudo & install vim (If put password on root)

```bash
su
/sbin/usermod -aG sudo $USER
apt install vim
```

### Edit vim [config](https://gitlab.com/stickman_0x00/dotfiles/-/blob/main/.vimrc)

```bash
vim ~/.vimrc
	...
sudo cp ~/.vimrc/root
```

Log out. Log in.

# Add sources

```bash
sudo vim /etc/apt/sources.list # add contrib non-free non-free-firmware
```

# Install packages

## Mate + i3wm

- blueman
- mate-applet-appmenu
- caja-open-terminal
- i3-wm rofi

## General

- linux-headers-amd64 nvidia-driver firmware-misc-nonfree
- vlc
- git
- obs-studio
- krita
- wireshark
- openssh-client
- qemu-system-x86
- libavcodec-extra
- gdb
- curl
- nload
- nmap
- glances
- build-essential
- fonts-noto-color-emoji fonts-firacode ttf-mscorefonts-installer
- docker docker-compose
- default-jre
- bat exa
- virt-manager
- lm-sensors
- systemd-resolved
- alacritty
- keepassxc
- transmission-gtk
- calibre
- kdenlive

## [Flatpak](https://flatpak.org/setup/Debian)

- org.mozilla.firefox org.freedesktop.Platform.ffmpeg-full
- us.zoom.Zoom
- com.spotify.Client
- com.github.tchx84.Flatseal
- com.stremio.Stremio
- org.gabmus.gfeeds

## [Syncthing](https://apt.syncthing.net/)

```bash
systemctl enable syncthing@$USER.service
```

## [GDB](https://github.com/hugsy/gef)

## Backports

```bash
vim /etc/apt/sources.list.d/backports.list
	deb http://deb.debian.org/debian bookworm-backports main
```

### Example

```bash
sudo apt install -t bookworm-backports package-name
```

# Purge

- plymouth
- xterm
- libreoffice-help-common firefox-esr
- plymouth
- pluma
- xpra

# Configurations

## I3 + Mate

```bash
gsettings set org.mate.session.required-components windowmanager i3
gsettings set org.mate.session.required-components filemanager ''
```

## Mate

### Hide desktop icons

```bash
gsettings set org.mate.background show-desktop-icons false
```

### Disable automount

```bash
gsettings set org.mate.media-handling automount false
```

### File Management Preferences

#### Views

- Show hidden files
- Show backup files

#### Preview

- Sound files > never

#### Media

- Never prompt or startup...

## Flatpak theme

```bash
sudo flatpak override --filesystem=$HOME/.themes
sudo flatpak override --filesystem=$HOME/.icons
```

## Flatpak firefox bad font

```
mkdir .var/app/org.mozilla.firefox/config/fontconfig
vim ~/.var/app/org.mozilla.firefox/config/fontconfig/fonts.conf
	<?xml version='1.0'?>
	<!DOCTYPE fontconfig SYSTEM 'fonts.dtd'>
	<fontconfig>
		<!-- Disable bitmap fonts. -->
		<selectfont><rejectfont><pattern>
			<patelt name="scalable"><bool>false</bool></patelt>
		</pattern></rejectfont></selectfont>
	</fontconfig>

```

## Remove quiet from grub

```bash
sudo vim /etc/default/grub
	GRUB_CMDLINE_LINUX_DEFAULT="" # quiet
```

```bash
sudo update-grub
```

## Install themes

- [WhiteSur](https://www.gnome-look.org/p/1403328)
- [WhiteSur icon](https://www.pling.com/p/1405756/) - dark

## Add groups to user

```bash
sudo usermod -aG wireshark $USER
sudo usermod -aG docker $USER
sudo usermod -aG libvirt $USER
```

## Enviroments

```bash
vim /etc/environment
	GTK_THEME=WhiteSur-Dark-solid
```

## DNS systemd-resolved

```bash
vim /etc/systemd/resolved.conf
	DNS=194.242.2.2#doh.mullvad.net 2a07:e340::2#doh.mullvad.net
	FallbackDNS=1.1.1.1#cloudflare-dns.com 1.0.0.1#cloudflare-dns.com 2606:4700:4700::1111#cloudflare-dns.com 2606:4700:4700::1001#cloudflare-dns.com
	DNSOverTLS=yes
systemctl enable systemd-resolved
```

### LightDM

Set theme and background image

```bash
sudo vim /etc/lightdm/lightdm-gtk-greeter.conf
```

```
[greeter]
background=/home/stickman/Pictures/Wallpaper/1.jpg
theme-name=Adwaita-dark
```

Set user list

```bash
sudo vim /etc/lightdm/lightdm.conf
```

```
[Seat:*]
greeter-hide-users=false
```
