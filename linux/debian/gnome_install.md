# After Install Standard Libraries

# Before start

## Add user to sudo & install vim (If put password on root)

```bash
su
/sbin/usermod -aG sudo $USER
apt install vim
```

### Edit vim [config](https://gitlab.com/stickman_0x00/dotfiles/-/blob/main/.vimrc)

```bash
vim ~/.vimrc
	...
sudo cp ~/.vimrc/root
```

Log out. Log in.

# Add sources

```bash
sudo vim /etc/apt/sources.list # add contrib non-free non-free-firmware
```

# Install packages

## Gnome

- gnome-shell
- gnome-terminal
- gnome-tweaks
- gnome-themes-extra
- nautilus nautilus-image-converter
- file-roller
- eog
- evince
- transmission-gtk
- qgnomeplatform-qt5
- gnome-disk-utility

## General

- vim
- htop
- git
- vlc
- libavcodec-extra
- obs-studio
- krita
- wireshark
- qemu-system-x86
- gdb
- curl
- nload
- nmap
- glances
- build-essential
- fonts-noto-color-emoji
- fonts-firacode
- docker docker-compose
- default-jre
- fwupd
- bat
- exa
- libreoffice-gnome hunspell-pt-pt ttf-mscorefonts-installer
- tlp

## [Flatpak](https://flatpak.org/setup/Debian)

- org.mozilla.firefox org.freedesktop.Platform.ffmpeg-full
- us.zoom.Zoom
- com.spotify.Client
- com.github.tchx84.Flatseal
- com.stremio.Stremio

## [Syncthing](https://apt.syncthing.net/)

```bash
systemctl enable syncthing@$USER.service
```

## [GDB](https://github.com/hugsy/gef)

## [Go](https://gitlab.com/stickman_0x00/scripts/-/raw/main/install_go.sh)

## Backports

```bash
vim /etc/apt/source.list.d/backports.list
	deb http://deb.debian.org/debian bookworm-backports main
```

### Example

```bash
sudo apt install -t bookworm-backports package-name
```

# Purge

- yelp
- plymouth
- xserver-xorg-video-intel
- im-config
- cron
- cron-daemon-common

## After

```bash
sudo apt autopurge
```

# Configurations

## Flatpak theme

```bash
sudo flatpak override --filesystem=$HOME/.themes
sudo flatpak override --filesystem=$HOME/.icons
```

## Remove quiet from grub

```bash
sudo vim /etc/default/grub
	GRUB_CMDLINE_LINUX_DEFAULT="" # quiet
```

```bash
sudo update-grub
```

## Install themes

- [WhiteSur](https://www.gnome-look.org/p/1403328)
- [WhiteSur icon](https://www.pling.com/p/1405756/) - dark

## Add groups to user

```bash
sudo usermod -aG wireshark $USER
sudo usermod -aG docker $USER
```

## Disable Services

- bluetooth.service
- fwupd-refresh.timer
- apt-daily.timer
- apt-daily-upgrade.timer
- docker

## Gnome

### Disable automount

```bash
gsettings set org.gnome.desktop.media-handling automount false
gsettings set org.gnome.desktop.media-handling automount-open false
```

### Nautilus always-use-location-entry

```bash
gsettings set org.gnome.nautilus.preferences always-use-location-entry true
```

### Extensions

- [User Themes](https://extensions.gnome.org/extension/19/user-themes/)
- [AppIndicator and KStatusNotifierItem Support](https://extensions.gnome.org/extension/615/appindicator-support/)
- [Just Perfection](https://extensions.gnome.org/extension/3843/just-perfection/)
- [Docker](https://extensions.gnome.org/extension/5103/docker/)
- [Dotspaces](https://extensions.gnome.org/extension/5347/dotspaces/)

## GDM

```bash
cd /etc/dconf
mkdir profile db/gdm.d

vim profile/gdm
	user-db:user
	system-db:gdm
	file-db:/usr/share/gdm/greeter-dconf-defaults

vim db/gdm.d/02-logo
	[org/gnome/login-screen]
	logo='/usr/share/pixmaps/mooncake.png'

vim db/gdm.d/03-scaling
	[org/gnome/desktop/interface]
	text-scaling-factor='1.25'

vim db/gdm.d/06-tap-to-click
	[org/gnome/desktop/peripherals/touchpad]
	tap-to-click=true

vim db/gdm.d/11-icon-settings
	[org/gnome/desktop/interface]
	icon-theme='WhiteSur-dark'

# Recompile GDM DB
dconf update
```

## Enviroments

```bash
mkdir .config/enviroment.d
vim .config/enviroment.d/envvars.conf
	GTK_THEME=WhiteSur-Dark
```

## DNS systemd-resolved

```bash
vim /etc/systemd/resolved.conf
	DNS=194.242.2.2#doh.mullvad.net 2a07:e340::2#doh.mullvad.net
	FallbackDNS=1.1.1.1#cloudflare-dns.com 1.0.0.1#cloudflare-dns.com 2606:4700:4700::1111#cloudflare-dns.com 2606:4700:4700::1001#cloudflare-dns.com
	DNSOverTLS=yes
systemctl enable systemd-resolved

vim /etc/NetworkManager/conf.d/dns.conf
	[main]
	dns=systemd-resolved
```
