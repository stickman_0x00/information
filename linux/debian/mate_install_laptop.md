# After Install Debian + Mate + Standard Libraries

## Add user to sudo & install vim (If put password on root)

```bash
su
/sbin/usermod -aG sudo stickman
apt install vim
vim /root/.vimrc
```

```
syntax on
set mouse=v
set ts=4
```

Log out. Log in.

## Sources

### Non free and contrib

```bash
sudo vim /etc/apt/sources.list # add contrib non-free
```

## Install packages

- vim
- firmware-linux-nonfree
- fwupd
- firejail
- mate-applet-appmenu - doesn't work in some apps
- [VSCodium](https://vscodium.com/) - follow instructions
- [Signal](https://www.signal.org/download/)
- git
- vlc
- obs-studio
- krita
- wireshark
- keepassxc
- openssh-client
- qemu
- qemu-system-x86
- gdb
- curl
- caja-open-terminal
- htop
- nload
- nmap
- build-essential (builds)
- libmpc-dev (build gcc)
- nasm
- dnscrypt-proxy
- fonts-noto-color-emoji
- fonts-firacode
- docker docker-compose
- default-jre
- virt-manager
- lm-sensors

### [Zoom](https://gitlab.com/stickman_0x00/scripts/-/raw/main/install_zoom.sh)

### [Go](https://gitlab.com/stickman_0x00/scripts/-/raw/main/install_go.sh)

### [GDB](https://github.com/hugsy/gef)

### [auto-cpufreq](https://github.com/AdnanHodzic/auto-cpufreq)

```bash
sudo auto-cpufreq --install
```

### [Syncthing](https://apt.syncthing.net/)

```bash
systemctl enable syncthing@$USER.service
```

### DNSCrypt-proxy

```bash
sudo vim /etc/NetworkManager/NetworkManager.conf
```

```
[main]
dns=none
```

```bash
sudo systemctl restart NetworkManager
sudo vim /etc/resolv.conf
```

```
nameserver 127.0.2.1
```

### Configure wireshark & libvirt

```bash
sudo usermod -aG wireshark $USER
sudo usermod -aG libvirt $USER
sudo usermod -aG docker $USER
```

### [Configure firejail sandbox for Zoom](https://github.com/alexjung/Run-Zoom-in-a-Sandbox)

```bash
sudo mkdir -p /opt/zoom/home
sudo chown -R $USER /opt/zoom
```

Only do this if didn't used the script to install

```bash
sudo mv /usr/bin/zoom /usr/bin/zoom.bak
sudo vim /usr/bin/zoom
```

```bash
#!/bin/bash
firejail --profile=/etc/firejail/zoom.profile --private=/opt/zoom/home -c -- /opt/zoom/ZoomLauncher "$@"
```

```bash
sudo chmod +x /usr/bin/zoom
```

Note: If zoom is already open and we open a zoom link session, zoom bugs.

"Solutions":

- copy id and password and join manually
- exit zoom and open link

Disable added services

- Disable "im-launch" from Startup Applications Preferences

- Disable ibus

```bash
sudo mv /usr/bin/ibus-daemon /usr/bin/ibus-daemon.bak
sudo vim /usr/bin/ibus-daemon
```

```
#!/bin/bash
```

```
sudo chmod --reference=/usr/bin/ibus /usr/bin/ibus-daemon
```

## Install firefox from mozilla binaries

go to [firefox](https://www.mozilla.org/en-US/firefox/download/thanks/); download

```bash
cd Downloads
tar -C ~/.local/ -xf firefox-<version>.tar.bz2
```

```bash
mkdir ~/.local/share/applications
vim ~/.local/share/applications/firefox-stable.desktop
```

```
[Desktop Entry]
Name=Firefox
Comment=Web Browser
Exec=/home/stickman/.local/firefox/firefox %u
Terminal=false
Type=Application
Icon=/home/stickman/.local/firefox/browser/chrome/icons/default/default128.png
Categories=Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/vnd.mozilla.xul+xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;
StartupNotify=true
Actions=Private;

[Desktop Action Private]
Exec=/home/stickman/.local/firefox/firefox --private-window %u
Name=Open in private mode
```

### Symlink and make it default

```bash
sudo update-alternatives --install /usr/bin/x-www-browser x-www-browser /home/stickman/.local/firefox/firefox 200 && sudo update-alternatives --set x-www-browser /home/stickman/.local/firefox/firefox
sudo ln -s /home/stickman/.local/firefox/firefox /usr/local/bin/firefox
```

## Purge packages

- xserver-xorg-video-intel
- amd64-microcode
- xterm
- libreoffice-help-common firefox-esr
- plymouth
- pluma
- xpra

## Layout

### Remove quiet from grub

```bash
sudo vim /etc/default/grub
```

```
GRUB_CMDLINE_LINUX_DEFAULT="" # quiet
```

```bash
sudo update-grub
```

### Themes

- [WhiteSur](https://www.gnome-look.org/p/1403328)
- [WhiteSur icon](https://www.pling.com/p/1405756/) - dark

> Add Global Application Menu applet to bar

Workspaces icons: 🖥️ > 🌐 > 📦 > 🔗

#### Terminal

Create a new terminal profile and then

```bash
bash -c "$(wget -qO- https://git.io/vQgMr)"
```

### Fonts

- [Go](https://go.googlesource.com/image/+/master/font/gofont/ttfs/) - ~/.local/share/fonts
- [Go Powerline](https://github.com/powerline/fonts/raw/master/GoMono/Go%20Mono%20for%20Powerline.ttf) - for terminal

## Disable Services

- bluetooth.service
- fwupd-refresh.timer
- apt-daily.timer
- apt-daily-upgrade.timer
- docker
- cups cups-browsed
- libvirtd libvirtd-admin libvirt-guests
- lm-sensors

## Mate

### Hide desktop icons

```bash
gsettings set org.mate.background show-desktop-icons false
```

### Disable automount

```bash
gsettings set org.mate.media-handling automount false
```

### File Management Preferences

#### Views

- Show hidden files
- Show backup files

#### Preview

- Sound files > never

#### Media

- Never prompt or startup...

## User friendly

### Tab completion

```bash
sudo vim ~/.inputrc
```

```
set completion-ignore-case on
```

### LightDM

Set theme and background image

```bash
sudo vim /etc/lightdm/lightdm-gtk-greeter.conf
```

```
[greeter]
background=/home/stickman/Pictures/Wallpaper/1.jpg
theme-name=Adwaita-dark
```

Set user list

```bash
sudo vim /etc/lightdm/lightdm.conf
```

```
[Seat:*]
greeter-hide-users=false
```
