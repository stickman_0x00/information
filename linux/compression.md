# Compression

# Zip files by month

```bash
>find . -type f -printf '7z a ola_%TY%Tm %f\n' | sh
```

# Extract multiple files to new folders

```bash
>for archive in *.zip; do 7z x -o"`echo $(echo "$archive" | cut -f 1 -d '.')`" "$archive"; done
```