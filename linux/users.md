# Users

# Create User

```bash
# Some distros
useradd -G sudo -m $USER -s /bin/bash
# Arch Linux
useradd -G wheel -m $USER -s /bin/bash

# change password
passwd $USER
```

## Add user to group

```bash
gpasswd -a $USER $GROUP
usermod -aG $GROUP $USER
```
