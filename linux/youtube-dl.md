# Youtube-dl

# Download audio playlist from youtube

```bash
youtube-dl --extract-audio --audio-format mp3 --embed-thumbnail --prefer-ffmpeg https://www.youtube.com/playlist?list=PLqSWkdF4vYFtRdaVgfMNsg-zy4UIeJwY4
```