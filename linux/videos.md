# Videos

# Convert mkv to mp4

```bash
ffmpeg -i input.mkv -codec copy output.mp4
```

# Reduce video size

```bash
ffmpeg -i input.mp4 -vcodec libx265 -crf 28 output.mp4
```

# Extract frames from a video, from 0 to 10 sec

```bash
ffmpeg -ss 00:00 -i input -t 00:10 filename%05d.png
```

# Trim

```bash
ffmpeg -i file.mkv -ss 00:00:20 -to 00:00:40 -c copy file-2.mkv
```
