# - Arch Install Desktop - GPT/UEFI/LVM/LUKS/GNOME Wayland/NVIDIA

## 1 - Startup

### 1.1 - Change keyboard settings (pt) (Optional)

```bash
loadkeys pt-latin1
```

### 1.2 - Check uefi exist

```bash
ls /sys/firmware/efi/efivars
```

#### 1.3.2 - Active ssh (Optional)

```bash
passwd
systemctl start sshd
```

### 1.4 - Format disk

```bash
# pick
# 1. zeroing disk
dd if=/dev/zero of=/dev/nvme0n1 bs=4M status=progress conv=fdatasync
# 2. fill with random data
dd if=/dev/urandom of=/dev/nvme0n1 bs=4M status=progress conv=fdatasync
# 3. Bonus paranoid - format 7 times with random data
for n in `seq 7`; do dd if=/dev/urandom of=/dev/sda bs=8b conv=notrunc; done
```

## 2 - Partitions

### 2.1 - Partions the disk

| Disks          | Size     | Porpuse  |
| -------------- |---------:|---------:|
| /dev/nvme0n1p1 | 512M     | Boot     |
| /dev/nvme0n1p2 | 100%FREE | Sytem    |

```bash
fdisk /dev/nvme0n1
# g
# n, enter, enter, +512M
# t, 1
# n, enter, enter, enter
# t, enter, 30
# w
```

### 2.2 - Format boot partion

```bash
mkfs.fat -F32 /dev/nvme0n1p1
```

### 2.3 - Encrypt disk partition

#### 2.3.1 - Encrypt disk partition

```bash
cryptsetup luksFormat /dev/nvme0n1p2
```

#### 2.3.2 - Open Encrypt disk partition with LVM

```bash
cryptsetup open --type luks /dev/nvme0n1p2 lvm
```

#### 2.3.3 - Create physical Volume

```bash
pvcreate --dataalignment 1m /dev/mapper/lvm
```

##### 2.3.4 - Create Volume Group

```bash
vgcreate vol_group_0 /dev/mapper/lvm
```

##### 2.3.5 - Create  Logical Partitions

| Partion          | Size     | Porpuse  |
| ---------------- |---------:|---------:|
| swap          | 32G      | SWAP     |
| root          | 64G      | ROOT     |
| home          | 100%FREE | HOME     |

```bash
lvcreate -L64G vol_group_0 -n lv_swap
lvcreate -L64G vol_group_0 -n lv_root
lvcreate -l 100%FREE vol_group_0 -n lv_home
```

##### 2.3.6 - Format Logical Partition

```bash
mkswap /dev/vol_group_0/lv_swap
mkfs.ext4 /dev/vol_group_0/lv_root
mkfs.ext4 /dev/vol_group_0/lv_home
```

#### 2.4 - Mounts volumes

```bash
mount /dev/vol_group_0/lv_root /mnt
mkdir /mnt/{boot,home}
mount -o uid=0,gid=0,fmask=0077,dmask=0077 /dev/nvme0n1p1 /mnt/boot
mount /dev/vol_group_0/lv_home /mnt/home
mkdir /mnt/etc
swapon /dev/vol_group_0/lv_swap
genfstab -U -p /mnt >> /mnt/etc/fstab
```

## 3 - First part of installation

### 3.1 - Change default url mirror & change pacman conf use parallel downloads

```bash
reflector --save /etc/pacman.d/mirrorlist --protocol https --country Portugal,Germany --latest 5 --sort age
vim /etc/pacman.conf
 Color
 VerbosePkgLists
 ParallelDownloads = 7
 ILoveCandy

 [multilib]
 Include = /etc/pacman.d/mirrorlist
```

### 3.2 - Install base

```bash
pacstrap -i /mnt base vim reflector
```

### 3.3 - arch-chroot

```bash
arch-chroot /mnt
```

### 3.3.1 Repeat step [3.1](#31---Change-default-url-mirror-to-first-position)

## 3.4 - Install initial packages

```bash
# needed
pacman -S base-devel lvm2
# needed optional
pacman -S amd-ucode bash-completion terminus-font linux-firmware ntfs-3g
# normal
pacman -S linux-zen linux-zen-headers
# lts
pacman -S linux-lts linux-lts-headers
# network
pacman -S networkmanager
systemctl enable NetworkManager
```

## 3.5 - Locale config

```bash
vim /etc/locale.gen
 en_US.UTF-8 UTF-8
locale-gen
locale > /etc/locale.conf
```

## 3.6 - Keyboard layout and font size on tty

```bash
vim /etc/vconsole.conf
 KEYMAP=pt-latin1
 FONT=ter-132n
```

## 3.7 - Set hosts and hostname

```bash
echo arch > /etc/hostname
vim /etc/hosts
 127.0.0.1 localhost
 ::1   localhost
 127.0.1.1 arch.localdomain arch
```

## 3.8 - Change root password

```bash
passwd
```

## 3.9 - Add user and give sudo privilege

```bash
useradd -m -g users -G wheel $user
passwd $user
# give sudo privilege
visudo
 ## Uncomment to allow members of group wheel to execute any command
 %wheel ALL=(ALL) ALL
```

## 3.10 - Change mkinitcpio to support lvm and encryption, generate mkinitcpio + Intel Modules

```bash
# add encrypt lvm2
# remove kms
vim /etc/mkinitcpio.conf
 HOOKS=(base udev autodetect modconf keyboard keymap consolefont block encrypt lvm2 filesystems fsck)
mkinitcpio -P
```

## 3.11 - Boot config

```bash
bootctl --path=/boot install
vim /boot/loader/loader.conf
 default arch_zen.conf
 timeout 3
 editor 0
# Get UID on vim - :read !blkid /dev/nvme0n1p2
vim /boot/loader/entries/arch_lts.conf
 title Arch Linux LTS
 linux /vmlinuz-linux-lts
 initrd /amd-ucode.img
 initrd /initramfs-linux-lts.img
 options cryptdevice=UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx:volume root=/dev/mapper/vol_group_0-lv_root rw
vim /boot/loader/entries/arch_zen.conf
 title Arch Linux ZEN
 linux /vmlinuz-linux-zen
 initrd /amd-ucode.img
 initrd /initramfs-linux-zen.img
 options cryptdevice=UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx:volume root=/dev/mapper/vol_group_0-lv_root rw
 options nvidia-drm.modeset=1
```

```bash
options pv6.disable=1 # if you want to disable ipv6
```

## 3.12 - Finish first part (REBOOT)

```bash
exit
umount /mnt/boot
umount /mnt/home
umount /mnt
reboot
```

# 4 - Second part of installation

## 4.1 - Timezone

```bash
# timedatectl list-timezones # list time zones
timedatectl set-timezone Portugal
hwclock --systohc # can't on raspberry
systemctl enable systemd-timesyncd
```

## 4.2 - Graphic environment

### 4.2.1 - DE

#### 4.2.1.1 - Gnome

```bash
pacman -S gdm gnome-shell gnome-control-center gnome-bluetooth-3.0 gnome-keyring gnome-tweaks malcontent xdg-user-dirs-gtk gnome-terminal

systemctl enable gdm.service
```

#### 4.2.1.2 - Mate

```bash
pacman -S mate mate-extras i3-wm lightdm-gtk-greeter xcompmgr
```

### 4.2.2 - Set graphical target

```bash
systemctl set-default graphical.target
```

## 4.3 - Install Programs

### 4.3.1 - All

| Name                                 | Description          |
|--------------------------------------|---------------------:|
| nautilus                             |                      |
| nautilus-image-converter             |                      |
| python-nautilus                      |                      |
| file-roller                          |                      |
| man                                  |                      |
| vlc                                  | video/audio          |
| spotify-launcher      |      |
| nmap                                 |                      |
| libreoffice-still                    |                      |
| hunspell-en_us      | |
| firefox                              | browser              |
| git                                  |                      |
| go                                   |                      |
| rsync | |
| wireshark-qt                         |                      |
| keepassxc                            | password manager     |
| wget                                 |                      |
| transmission-gtk                     |                      |
| htop                                 | process monitor      |
| nload                                | network monitor      |
| glances                              | multi monitor        |
| krita                                | "photoshop"          |
| pinta
| obs-studio                           |                      |
| android-tools
| gvfs-mtp                             | android file manager |
| gvfs-smb                              | samba share         |
| bluez                                |                      |
| bluez-utils                          |                      |
| docker                               |                      |
| docker-compose                       |                      |
| qt5-wayland                          |                      |
| qt6-wayland                          |                      |
| qgnomeplatform-qt5                   |                      |
| qgnomeplatform-qt6                   |                      |
| glfw-wayland                         |                      |
| gst-plugins-bad                      |                      |
| dconf-editor                         |                      |
| eog                                  |                      |
| signal-desktop                       |                      |
| polari                               |                      |
| qemu-full                            |                      |
| gdb                                  |                      |
| aarch64-linux-gnu-gdb                |                      |
| gef (+optional dependencies)         |                      |
| pwndbg (+optional dependencies)      |                      |
| rustup                               |                      |
| rust-analyzer                        |                      |
| aarch64-linux-gnu-binutils           |                      |
| jre-openjdk                          |                      |
| capitaine-cursors                    |                      |
| pacman-contrib                       |                      |
| bat
| eza                                  |                      |
| syncthing                            |                      |
| nvidia-open-dkms                          |                      |
| nvidia-utils                         |                      |
| nvidia-settings                      |                      |
| lib32-nvidia-utils                   |                      |
| libva-nvidia-driver                       | |
| steam                                |                      |
| chromium                             | because of minecraft login |
| libvirt        | virtualization |
| virt-manager       | virtualization |
| iptables-nft       | virtualization |
| dnsmasq        | virtualization |
| swtpm         | virtualization |

## 4.3.2 - Audio / Screen share

| Name                         | Description          | Check if needed |
|------------------------------|---------------------:|:---------------:|
| wireplumber                  |                      |                 |
| pipewire-jack                |                      |                 |
| pipewire-pulse               |                      |                 |
| pipewire-alsa                |                      |                 |
| gst-plugin-pipewire          |                      |                 |
| gst-plugins-good             |                      |                 |
| lib32-pipewire
| lib32-pipewire-jack

### 4.3.3 - Fonts

- gnu-free-fonts
- ttf-hack
- ttf-liberation
- noto-fonts-emoji
- ttf-droid
- ttf-fira-code
- ttf-dejavu
- noto-fonts
- ttf-ubuntu-font-family

# 5 - Configuration

## 5.1 - Network

### 5.1.1 - DNS systemd

```bash
vim /etc/systemd/resolved.conf
 DNS=9.9.9.9#dns.quad9.net 149.112.112.112#dns.quad9.net 2620:fe::fe#dns.quad9.net 2620:fe::9#dns.quad9.net
 FallbackDNS=1.1.1.1#cloudflare-dns.com 9.9.9.9#dns.quad9.net 8.8.8.8#dns.google 2606:4700:4700::1111#cloudflare-dns.com 2620:fe::9#dns.quad9.net 2001:4860:4860::8888#dns.google
 DNSOverTLS=yes
systemctl enable systemd-resolved

vim /etc/NetworkManager/conf.d/dns.conf
 [main]
 dns=systemd-resolved
```

## 5.2 - Services

### 5.2.1 - Enable services

```bash
systemctl enable fstrim.timer
systemctl enable syncthing@$USER.service
systemctl enable bluetooth.service
systemctl enable libvirtd.service
```

## 5.2.1 - NVIDIA GPU

```bash
vim /etc/environment
 LIBVA_DRIVER_NAME=nvidia
 VDPAU_DRIVER=nvidia

mkdir /etc/pacmand.d/hooks
vim /etc/pacman.d/hooks/nvidia.hook
 [Trigger]
 Operation=Install
 Operation=Upgrade
 Operation=Remove
 Type=Package
 Target=nvidia-dkms
 Target=linux-zen
 # Change the linux part above and in the Exec line if a different kernel is used

 [Action]
 Description=Update NVIDIA module in initcpio
 Depends=mkinitcpio
 When=PostTransaction
 NeedsTargets
 Exec=/bin/sh -c 'while read -r trg; do case $trg in linux) exit 0; esac; done; /usr/bin/mkinitcpio -P'
```

### 5.2.1.1 - If the option of wayland doesn't appear on GDM

```bash
ln -s /dev/null /etc/udev/rules.d/61-gdm.rules
```

## 5.3 - Env VARS

```bash
vim .config/environment.d/envvars.conf
 GDK_BACKEND="wayland,x11"
 GTK_THEME=WhiteSur-Dark

 CLUTTER_BACKEND=wayland

 SDL_VIDEODRIVER=wayland

 VDPAU_DRIVER=va_gl

 MOZ_ENABLE_WAYLAND=1

 QT_QPA_PLATFORM="wayland;xcb"
 QT_QPA_PLATFORMTHEME=gnome

vim .config/electron13-flags.conf
 --enable-features=WaylandWindowDecorations
 --ozone-platform-hint=auto
```

## 5.4 - Virtualization - QEMU/Virt Manager

```bash
vim /etc/libvirt/libvirt.conf
 uri_default = "qemu:///system"

vim /etc/libvirt/libvirtd.conf
 unix_sock_group = "libvirt"
 unix_sock_ro_perms = "0777"
 unix_sock_rw_perms = "0770"
```

## 5.5 - Add user to groups

```bash
gpasswd -a $USER wireshark
gpasswd -a $USER docker
gpasswd -a $USER libvirt
```

## 5.6 - Gnome

### 5.6.1 - Extensions

- [User Themes](https://extensions.gnome.org/extension/19/user-themes/)
- [AppIndicator and KStatusNotifierItem Support](https://extensions.gnome.org/extension/615/appindicator-support/)
- [Just Perfection](https://extensions.gnome.org/extension/3843/just-perfection/)
- [Docker](https://extensions.gnome.org/extension/5103/docker/)
- [Aylur's Widgets](https://extensions.gnome.org/extension/5338/aylurs-widgets/)

### 5.6.2 - Gsettings

#### 5.6.2.1 - GDM

```bash
cd /etc/dconf
mkdir profile db/gdm.d

vim profile/gdm
 user-db:user
 system-db:gdm
 file-db:/usr/share/gdm/greeter-dconf-defaults

vim db/gdm.d/02-logo
 [org/gnome/login-screen]
 logo='/usr/share/pixmaps/archlinux-logo.svg'

vim db/gdm.d/03-scaling
 [org/gnome/desktop/interface]
 text-scaling-factor='1.25'

vim db/gdm.d/06-tap-to-click
 [org/gnome/desktop/peripherals/touchpad]
 tap-to-click=true

vim db/gdm.d/11-icon-settings
 [org/gnome/desktop/interface]
 icon-theme='WhiteSur-dark'

# Recompile GDM DB
dconf update
```

### 5.6.3 - Change .desktop files

```bash
cp /usr/share/applications/... ~/.local/share/applications/
vim ~/.local/share/applications/...
vim.desktop
org.gnome.eog.desktop
vlc.desktop
engrampa.desktop
 NoDisplay=true

cp /usr/share/applications/... ~/.local/share/applications/
vim ~/.local/share/applications/...
com.obsproject.Studio.desktop
org.wireshark.Wireshark.desktop
org.keepassxc.KeePassXC.desktop
 Exec=env QT_QPA_PLATFORM=xcb $PROGRAM
```

### 5.6.4 - Themes

- [WhiteSur](https://www.gnome-look.org/p/1403328)
- [WhiteSur icon](https://www.pling.com/p/1405756/) - dark

### 5.6.5 - Fonts

- [Go](https://go.googlesource.com/image/+/master/font/gofont/ttfs/) - ~/.local/share/fonts
- [Go Powerline](https://github.com/powerline/fonts/raw/master/GoMono/Go%20Mono%20for%20Powerline.ttf) - for terminal

### 5.6.6 - Change gnome-terminal theme

First create a new profile on the gnome-terminal and then [Gogh](https://github.com/Mayccoll/Gogh).

## 5.7 - Rust

```bash
rustup default stable
```

# 6 - Aur

## 6.1 - MAKEPKG use all cores

```bash
vim /etc/makepkg.conf
 MAKEFLAGS="-j$(nproc)"
```

## 6.2 - Install AUR Helper - [YAY](https://aur.archlinux.org/packages/yay)

```bash
git clone # see the git on aur package of aur helper
cd yay
makepkg -si # install
```

## 6.3 - Programs

| Name                           | Description          |
|--------------------------------|---------------------:|
| vscodium-bin                   |                      |
| mkinitcpio-firmware            |                      |
| minecraft-launcher             |                      |
| nvidia-vaapi-driver-git        | Fix nvidia |

## 6.4 - Config

### 6.4.1 - VSCodium

```bash
vim .config/code-flags.conf
 --enable-features=WaylandWindowDecorations
 --ozone-platform-hint=auto
```

# Notes

1. Keepassxc auto-type not working. (QT_QPA_PLATFORM=xcb or QT_QPA_PLATFORMTHEME=qt5ct works)
