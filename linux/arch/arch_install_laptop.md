# XPS 13 9370 - Arch Install Laptop - GPT/UEFI/LVM/LUKS/GNOME

# 1 - Startup

## 1.1 - Change keyboard settings (pt) (Optional)

```bash
loadkeys pt-latin1
```

## 1.2 - Check uefi exist

```bash
ls /sys/firmware/efi/efivars
```

## 1.3 - Network

### 1.3.1 - WIFI

```bash
iwctl
	station $wireless_device scan
	station $wireless_device get-networks
	station $wireless_device connect $wireless_network
	ctrl + d
ping google.com
```

### 1.3.2 - Initiate SSH

```bash
passwd
systemctl start sshd
```

## 1.4 - Format disk

```bash
# pick
# 1. zeroing disk
dd if=/dev/zero of=/dev/nvme0n1 bs=4M status=progress conv=fdatasync
# 2. fill with random data
dd if=/dev/urandom of=/dev/nvme0n1 bs=4M status=progress conv=fdatasync
# 3. Bonus paranoid - format 7 times with random data
for n in `seq 7`; do dd if=/dev/urandom of=/dev/sda bs=8b conv=notrunc; done
```

# 2 - Partitions

## 2.1 - Partions the disk

| Disks          |     Size | Porpuse |
| -------------- | -------: | ------: |
| /dev/nvme0n1p1 |     512M |    Boot |
| /dev/nvme0n1p2 | 100%FREE |   Sytem |

```bash
fdisk /dev/nvme0n1
# g
# n, enter, enter, +512M
# t, 1
# n, enter, enter, enter
# t, enter, 30
# w
```

## 2.2 - Format boot partion

```bash
mkfs.fat -F32 /dev/nvme0n1p1
```

## 2.3 - Encrypt disk partition

### 2.3.1 - Encrypt disk partition

```bash
cryptsetup luksFormat /dev/nvme0n1p2
```

### 2.3.2 - Open Encrypt disk partition with LVM

```bash
cryptsetup open --type luks /dev/nvme0n1p2 lvm
```

### 2.3.3 - Create physical Volume

```bash
pvcreate --dataalignment 1m /dev/mapper/lvm
```

### 2.3.4 - Create Volume Group

```bash
vgcreate vol_group_0 /dev/mapper/lvm
```

### 2.3.5 - Create Logical Partitions

| Partion |     Size | Porpuse |
| ------- | -------: | ------: |
| swap    |      16G |    SWAP |
| root    |      64G |    ROOT |
| home    | 100%FREE |    HOME |

```bash
lvcreate -L16G vol_group_0 -n lv_swap
lvcreate -L64G vol_group_0 -n lv_root
lvcreate -l 100%FREE vol_group_0 -n lv_home
```

### 2.3.6 - Format Logical Partition

```bash
mkswap /dev/vol_group_0/lv_swap
mkfs.ext4 /dev/vol_group_0/lv_root
mkfs.ext4 /dev/vol_group_0/lv_home
```

## 2.4 - Mounts volumes

```bash
mount /dev/vol_group_0/lv_root /mnt
mkdir /mnt/{boot,home}
mount /dev/nvme0n1p1 /mnt/boot
mount /dev/vol_group_0/lv_home /mnt/home
mkdir /mnt/etc
swapon /dev/vol_group_0/lv_swap
genfstab -U -p /mnt >> /mnt/etc/fstab
```

# 3 - First part of installation

## 3.1 - Change default url mirror & change pacman conf use parallel downloads

```bash
reflector --save /etc/pacman.d/mirrorlist --protocol https --country Portugal,Germany --latest 5 --sort age
vim /etc/pacman.conf
	Color
	VerbosePkgLists
	ParallelDownloads = 7
	ILoveCandy
```

## 3.2 - Install base

```bash
pacstrap -i /mnt base vim reflector
```

## 3.3 - arch-chroot

```bash
arch-chroot /mnt
```

### 3.3.1 Repeat step [3.1](#31---Change-default-url-mirror-to-first-position)

## 3.4 - Install initial packages

```bash
# needed
pacman -S base-devel lvm2
# needed optional
pacman -S intel-ucode bash-completion terminus-font linux-firmware
# lts
pacman -S linux-lts linux-lts-headers
# wifi
pacman -S networkmanager
systemctl enable NetworkManager
```

## 3.5 - Locale config

```bash
vim /etc/locale.gen
    en_US.UTF-8 UTF-8
locale-gen
locale > /etc/locale.conf
```

## 3.6 - Keyboard layout and font size on tty

```bash
vim /etc/vconsole.conf
    KEYMAP=pt-latin1
    FONT=ter-132n
```

## 3.7 - Set hosts and hostname

```bash
echo arch > /etc/hosts
vim /etc/hosts
    127.0.0.1	localhost
    ::1			localhost
    127.0.1.1	arch.localdomain	arch
```

## 3.8 - Change root password

```bash
passwd
```

## 3.9 - Add user and give sudo privilege

```bash
useradd -m -g users -G wheel $user
passwd $user
# give sudo privilege
visudo
    ## Uncomment to allow members of group wheel to execute any command
    %wheel ALL=(ALL) ALL
```

## 3.10 - Change mkinitcpio to support lvm and encryption, generate mkinitcpio + Intel Modules

```bash
# add encrypt lvm2
vim /etc/mkinitcpio.conf
    MODULES=(intel_agp i915)
    HOOKS=(base udev autodetect modconf kms keyboard keymap consolefont block encrypt lvm2 filesystems fsck)
mkinitcpio -P
```

## 3.11 - Boot config

```bash
bootctl --path=/boot install
vim /boot/loader/loader.conf
    default arch.conf
    timeout 3
    editor 0
# Get UID on vim - :read !blkid /dev/nvme0n1p2
vim /boot/loader/entries/arch_lts.conf
    title Arch Linux LTS
    linux /vmlinuz-linux-lts
    initrd /intel-ucode.img
    initrd /initramfs-linux-lts.img
    options cryptdevice=UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx:volume root=/dev/mapper/vol_group_0-lv_root rw
vim /boot/loader/entries/arch.conf
	title Arch Linux
	linux /vmlinuz-linux
	initrd /intel-ucode.img
	initrd /initramfs-linux.img
	options cryptdevice=UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx:volume root=/dev/mapper/vol_group_0-lv_root rw
```

## 3.12 - Finish first part (REBOOT)

```bash
exit
umount /mnt/boot
umount /mnt/home
umount /mnt
reboot
```

# 4 - Second part of installation

## 4.1 - Timezone

```bash
# timedatectl list-timezones # list time zones
timedatectl set-timezone Portugal
hwclock --systohc # can't on raspberry
systemctl enable systemd-timesyncd
```

## 4.2 - Graphic environment

### 4.2.1 - Gnome

```bash
pacman -S gdm gnome-shell gnome-control-center gnome-bluetooth-3.0 gnome-keyring gnome-tweaks malcontent xdg-user-dirs-gtk gnome-terminal

systemctl enable gdm.service
systemctl set-default graphical.target
```

## 4.3 - Install Programs

### 4.3.1 - All

| Name                            | Description                 |
| ------------------------------- | --------------------------- |
| nautilus                        |                             |
| nautilus-image-converter        |                             |
| python-nautilus                 |                             |
| file-roller                     |                             |
| man                             |                             |
| vlc                             | video/audio                 |
| spotify-launcher                |                             |
| nmap                            |                             |
| libreoffice-still               |                             |
| hunspell-en_us                  |
| firefox                         | browser                     |
| git                             |                             |
| go                              |                             |
| wireshark-qt                    |                             |
| keepassxc                       | password manager            |
| wget                            |                             |
| transmission-gtk                |                             |
| htop                            | process monitor             |
| nload                           | network monitor             |
| glances                         | multi monitor               |
| krita                           | "photoshop"                 |
| obs-studio                      |                             |
| gvfs-mtp                        | android file manager        |
| gvfs-smb                        | samba share                 |
| fwupd                           | update bios                 |
| bluez                           |                             |
| bluez-utils                     |                             |
| docker                          |                             |
| docker-compose                  |                             |
| adwaita-qt5                     |                             |
| adwaita-qt6                     |                             |
| qt5-wayland                     |                             |
| qt6-wayland                     |                             |
| qgnomeplatform-qt5              |                             |
| qt5-wayland                     |                             |
| qt6-wayland                     |                             |
| glfw-wayland                    |                             |
| vulkan-intel                    |                             |
| intel-media-driver              | intel hardware accelaration |
| libvdpau-va-gl                  | intel hardware accelaration |
| gstreamer-vaapi                 |                             |
| gst-plugins-bad                 |                             |
| dconf-editor                    |                             |
| eog                             |                             |
| signal-desktop                  |                             |
| polari                          |                             |
| qemu-full                       |                             |
| gdb                             |                             |
| aarch64-linux-gnu-gdb           |                             |
| gef (+optional dependencies)    |                             |
| pwndbg (+optional dependencies) |                             |
| rustup                          |                             |
| rust-analyzer                   |                             |
| aarch64-linux-gnu-binutils      |                             |
| jre-openjdk                     |                             |
| capitaine-cursors               |                             |
| pacman-contrib                  |                             |
| exa                             |                             |

| bat
| syncthing git
| go
| ntfs-3g

## 4.3.2 - Audio / Screen share

| Name                | Description | Check if needed |
| ------------------- | ----------: | :-------------: |
| wireplumber         |             |                 |
| pipewire-jack       |             |                 |
| pipewire-pulse      |             |                 |
| pipewire-alsa       |             |                 |
| gst-plugin-pipewire |             |                 |
| gst-plugins-good    |             |                 |

### 4.3.3 - Laptop

| Name    | Description | Check if needed |
| ------- | ----------: | :-------------: |
| tlp     |             |                 |
| tlp-rdw |             |                 |

### 4.3.4 - Fonts

-   gnu-free-fonts
-   ttf-hack
-   ttf-liberation
-   noto-fonts-emoji
-   ttf-droid
-   ttf-fira-code
-   ttf-dejavu
-   noto-fonts
-   ttf-ubuntu-font-family

# 5 - Configuration

## 5.1 - Network

### 5.1.1 - DNS systemd

```bash
vim /etc/systemd/resolved.conf
    DNS=9.9.9.9#dns.quad9.net 149.112.112.112#dns.quad9.net 2620:fe::fe#dns.quad9.net 2620:fe::9#dns.quad9.net
	FallbackDNS=1.1.1.1#cloudflare-dns.com 9.9.9.9#dns.quad9.net 8.8.8.8#dns.google 2606:4700:4700::1111#cloudflare-dns.com 2620:fe::9#dns.quad9.net 2001:4860:4860::8888#dns.google
    DNSOverTLS=yes
systemctl enable systemd-resolved

vim /etc/NetworkManager/conf.d/dns.conf
    [main]
    dns=systemd-resolved
```

## 5.2 - Services

### 5.2.1 - TLP

```bash
systemctl enable tlp
systemctl enable NetworkManager-dispatcher.service
systemctl mask systemd-rfkill.service systemd-rfkill.socket
```

### 5.2.2 - Enable services

```bash
systemctl enable fstrim.timer
systemctl enable syncthing@$USER.service
systemctl enable bluetooth.service
```

## 5.4 - Intel GPU

We previously already added the modules to mkinitcpio on step [3.1.1](#310---Change-mkinitcpio-to-support-lvm-and-encryption-generate-mkinitcpio--Intel-Modules).

```bash
vim /etc/modprobe.d/i915.conf
    options i915 enable_guc=2
    options i915 enable_fbc=1
```

## 5.5 - GDB

```bash
vim ~/.gdbinit
#source /usr/share/pwndbg/gdbinit.py
source /usr/share/gef/gef.py
```

## 5.6 - Tab completion folders

```bash
echo set completion-ignore-case on | sudo tee -a /etc/inputrc
```

## 5.7 - Env VARS

```bash
vim .config/environment.d/envvars.conf
    GDK_BACKEND="wayland,x11"
    GTK_THEME=WhiteSur-Dark

    CLUTTER_BACKEND=wayland

    SDL_VIDEODRIVER=wayland

    VDPAU_DRIVER=va_gl

    MOZ_ENABLE_WAYLAND=1

    QT_QPA_PLATFORM="wayland;xcb"
    QT_QPA_PLATFORMTHEME=gnome

vim .config/electron13-flags.conf
    --enable-features=WaylandWindowDecorations
    --ozone-platform-hint=auto
```

## 5.8 Change gnome-terminal theme

First create a new profile on the gnome-terminal and then [Gogh](https://github.com/Mayccoll/Gogh).

## 5.9 - Add user to groups

```bash
gpasswd -a $USER wireshark
gpasswd -a $USER docker
```

## 5.10 - Gnome

### 5.10.1 - Extensions

-   [User Themes](https://extensions.gnome.org/extension/19/user-themes/)
-   [AppIndicator and KStatusNotifierItem Support](https://extensions.gnome.org/extension/615/appindicator-support/)
-   [Just Perfection](https://extensions.gnome.org/extension/3843/just-perfection/)
-   [Docker](https://extensions.gnome.org/extension/5103/docker/)
-   [Aylur's Widgets](https://extensions.gnome.org/extension/5338/aylurs-widgets/)

### 5.10.2 - Gsettings

#### 5.10.2.1 - GDM

```bash
cd /etc/dconf
mkdir profile db/gdm.d

vim profile/gdm
    user-db:user
    system-db:gdm
    file-db:/usr/share/gdm/greeter-dconf-defaults

vim db/gdm.d/02-logo
    [org/gnome/login-screen]
    logo='/usr/share/pixmaps/archlinux-logo.svg'

vim db/gdm.d/03-scaling
    [org/gnome/desktop/interface]
    text-scaling-factor='1.25'

vim db/gdm.d/06-tap-to-click
    [org/gnome/desktop/peripherals/touchpad]
    tap-to-click=true

vim db/gdm.d/11-icon-settings
    [org/gnome/desktop/interface]
    icon-theme='WhiteSur-dark'

# Recompile GDM DB
dconf update
```

### 5.10.3 - Change .desktop files

```bash
cp /usr/share/applications/... ~/.local/share/applications/
vim ~/.local/share/applications/...
vim.desktop
org.gnome.eog.desktop
vlc.desktop
engrampa.desktop
    NoDisplay=true

cp /usr/share/applications/... ~/.local/share/applications/
vim ~/.local/share/applications/...
com.obsproject.Studio.desktop
org.wireshark.Wireshark.desktop
org.keepassxc.KeePassXC.desktop
	Exec=env QT_QPA_PLATFORM=xcb $PROGRAM
```

### 5.10.4 - Themes

-   [McMojave-circle](https://www.gnome-look.org/p/1305429) - blue
-   [McMojave](https://www.gnome-look.org/p/1275087/) - dark
-   [McMojave cursors](https://www.pling.com/p/1355701/)
-   [WhiteSur](https://www.gnome-look.org/p/1403328)
-   [WhiteSur icon](https://www.pling.com/p/1405756/) - dark

### 5.10.5 - Fonts

-   [Go](https://go.googlesource.com/image/+/master/font/gofont/ttfs/) - ~/.local/share/fonts
-   [Go Powerline](https://github.com/powerline/fonts/raw/master/GoMono/Go%20Mono%20for%20Powerline.ttf) - for terminal

## 5.11 - Bluetooth

### 5.11.1 [Power State](https://wiki.archlinux.org/title/bluetooth#Default_adapter_power_state)

```bash
vim /etc/bluetooth/main.conf
	[Policy]
	AutoEnable=false
```

# 6 - Aur

## 6.1 - MAKEPKG use all cores

```bash
vim /etc/makepkg.conf
    MAKEFLAGS="-j$(nproc)"
```

## 6.2 - Install AUR Helper - [YAY](https://aur.archlinux.org/packages/yay)

```bash
git clone # see the git on aur package of aur helper
cd yay
makepkg -si # install
```

## 6.3 - Programs

| Name                | Description | Check if needed |
| ------------------- | ----------: | :-------------: |
| vscodium-bin        |             |                 |
| zoom                |             |                 |
| mkinitcpio-firmware |             |                 |

## 6.4 - Config

### 6.4.1 - VSCodium

```bash
vim .config/code-flags.conf
    --enable-features=WaylandWindowDecorations
    --ozone-platform-hint=auto
```

### 6.4.2 - Zoom

```bash
vim ~/.config/zoomus.conf
    useSystemTheme=true
    enableWaylandShare=true

cp /usr/share/applications/Zoom.desktop ~/.local/share/applications/
vim ~/.local/share/applications/Zoom.desktop
	Exec=env QT_QPA_PLATFORM=xcb /usr/bin/zoom %U
```

# Notes

1. Zoom/Obs/Wireshark not using complete wayland version
2. Keepassxc auto-type not working. (QT_QPA_PLATFORM=xcb or QT_QPA_PLATFORMTHEME=qt5ct works)
