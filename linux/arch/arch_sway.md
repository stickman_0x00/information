
```bash
pacstrap -i /mnt base vim reflector bash-completion iptables-nft

pacman -S base-devel lvm2 amd-ucode linux-firmware ntfs-3g linux-zen linux-zen-headers mkinitcpio networkmanager nvidia-open-dkms nvidia-utils lib32-nvidia-utils nvidia-settings libvdpau-va-gl libva-nvidia-driver vulkan-validation-layers

pacman -S sway sway-contrib swaybg waybar xorg-xwayland xdg-desktop-portal-wlr xdg-desktop-portal-gnome ghostty rofi-wayland bluez bluez-utils blueman network-manager-applet gtk2 gtk3 gtk4 qt5-wayland qt6-wayland qt5ct qt6ct qt6-base qt5-base polkit xdg-user-dirs xdg-utils dex dunst nwg-look gnu-free-fonts

pacman -S ttf-hack ttf-liberation noto-fonts-emoji ttf-droid ttf-fira-code ttf-dejavu noto-fonts ttf-ubuntu-font-family ttf-font-awesome

pacman -S pipewire lib32-pipewire pipewire-audio pipewire-alsa pipewire-pulse pipewire-jack lib32-pipewire-jack pavucontrol

pacman -S firefox nautilus-image-converter nautilus-python ffmpegthumbnailer gst-libav gst-plugins-ugly man man-db vlc nmap libreoffice-still hunspell-en_us firefox rsync wireshark-qt keepassxc wget transmission-gtk htop nload glances krita obs-studio android-tools gvfs-mtp gvfs-smb eog signal-desktop pacman-contrib bat eza syncthing flatpak kdenlive kimageformats mediainfo noise-suppression-for-voice opencv python-openai-whisper python-srt_equalizer python-vosk qt6-imageformats qt6-multimedia-ffmpeg 7zip jre-openjdk newsflash


yay -S nautilus-open-any-terminal pokemon-colorscripts

```

```bash
sudo pacman -S steam prismlauncher
yay -S protontricks
```

```bash
sudo pacman -S npm webkit2gtk gdb gdb aarch64-linux-gnu-gdb gef pwndbg aarch64-linux-gnu-binutils docker docker-compose git go
yay -S visual-studio-code-bin
```

```bash
sudo pacman -S qemu-full libvirt virt-manager dnsmasq swtpm
```

sudo flatpak override --filesystem=$HOME/.themes
sudo flatpak override --filesystem=$HOME/.local/share/icons
gsettings set org.gnome.desktop.wm.preferences button-layout appmenu,maximize,minimize,close:

systemctl enable fstrim.timer syncthing@$USER.service bluetooth.service libvirtd.service paccache.timer

vim /etc/modprobe.d/hid_apple.conf
