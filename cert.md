# SSL Certificate

## Creating your CA Certificate (Certificate Authority)

### Prerequisites

#### 1. Check hostname

```bash
hostname
```

#### 2. Check FQDN

```bash
hostname -f
```

#### 3. Change FQDN if  Hostname == FQDN (repeat 1 and 2 to check again)

```bash
vim /etc/hosts
    127.0.1.1       debian.localdomain      debian
```

#### 4. Install/Configure NTP

### OpenSSL Configuration

```bash
cd /etc/ssl
cp openssl.cnf openssl.cnf.original # backup configuration
vim openssl.cnf # start configuration
    [ CA_default ]
    dir             = /root/ca              # Where everything is kept
```

/root/ca - where we are going to storethe keys

```bash
cd /root/
mkdir ca
cd ca
mkdir newcerts certs crl private requests
touch index.txt #  where OpenSSL keeps track of all signed certificates
echo 00 > serial # each signed certificate will have a serial number. I will start with number 1
```

### Root CA

#### Create root private key

```bash
openssl genrsa -aes256 -out /root/ca/private/cakey.pem 4096
```

#### Create self-signed root CA certificate with root private key

```bash
openssl req -new -x509 -key /root/ca/private/cakey.pem -out /root/ca/cacert.pem -days 3650
```

#### Verify this root CA certificate

```bash
openssl x509 -in ca.pem -text -noout
```

### Create a certificate for a web server

#### Create private key

```bash
openssl genrsa -out /root/ca/requests/testkey.pem 2048
```

#### Create CSR(Cert Signing Request)

```bash
openssl req -new -key /root/ca/requests/testkey.pem -out /root/ca/requests/test.csr
```

#### Sign the CSR

```bash
openssl ca -in /root/ca/requests/test.csr -out /root/ca/requests/test.pem
```

#### Clean

```bash
rm /root/ca/requests/test.csr
mv /root/ca/requests/testkey.pem /root/ca/private/
mv /root/ca/requests/test.pem /root/ca/certs/
```

### Security

```bash
chmod -R 600 /root/ca
```

### Selftcert dev

```bash
openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out MyCertificate.crt -keyout MyKey.key
```

### Sources

1. [networklessons](https://networklessons.com/uncategorized/openssl-certification-authority-ca-ubuntu-server)
2. [fabianlee](https://fabianlee.org/2018/02/17/ubuntu-creating-a-trusted-ca-and-san-certificate-using-openssl-on-ubuntu/)
