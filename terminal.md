# ANSI

Move cursor to begin of the line.

>\r

Move cursor to begin of the line and one line up.

>\033[F

Move cursor to top left corner of the screen.

>\033[H

Move cursor one line down.

>\n

Clears the part of the screen from the cursor to the end of the screen.

>\033[J

Clears the screen.

>\033[2J

# Shortcuts

Search history.

> Ctrl + r

Clean screen. (same has clear command).

> Ctrl + l

Delete word.

> Ctrl + w

Delete everything behind cursor.

> Ctrl + u
